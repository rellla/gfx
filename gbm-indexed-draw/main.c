#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

#include <fcntl.h>
#include <unistd.h>

#include <png.h>
#include <gbm.h>

#include <epoxy/gl.h>
#include <epoxy/egl.h>


GLuint program;
EGLDisplay display;
EGLSurface surface;
EGLContext context;
struct gbm_device *gbm;
struct gbm_surface *gs;

struct timespec start;
struct timespec end;

#define TARGET_W 512
#define TARGET_H 512

void eglCheckError(const char *stmt, const char *fname, int line) {
	EGLint err = eglGetError();
	if (err != EGL_SUCCESS)
		printf("EGL ERROR (0x%08x): %s failed at %s:%i\n", err, stmt, fname, line);
}

void glCheckError(const char *stmt, const char *fname, int line) {
	GLint err = glGetError();
	if (err != GL_NO_ERROR)
		printf("GL ERROR (0x%08x): %s failed at %s:%i\n", err, stmt, fname, line);
}

#define EGL_CHECK(stmt) do { \
	stmt; \
	eglCheckError(#stmt, __FILE__, __LINE__); \
	} while (0)

#define GL_CHECK(stmt) do { \
	stmt; \
	glCheckError(#stmt, __FILE__, __LINE__); \
	} while (0)

EGLConfig get_config(void)
{
	EGLint egl_config_attribs[] = {
		EGL_BUFFER_SIZE,	32,
		EGL_DEPTH_SIZE,		EGL_DONT_CARE,
		EGL_STENCIL_SIZE,	EGL_DONT_CARE,
		EGL_SURFACE_TYPE,	EGL_WINDOW_BIT,
		EGL_RENDERABLE_TYPE,	EGL_OPENGL_ES2_BIT,
		EGL_NONE,
	};

	EGLint num_configs;
	EGL_CHECK(assert(eglGetConfigs(display, NULL, 0, &num_configs) == EGL_TRUE));

	EGLConfig *configs = malloc(num_configs * sizeof(EGLConfig));
	EGL_CHECK(assert(eglChooseConfig(display, egl_config_attribs,
			       configs, num_configs, &num_configs) == EGL_TRUE));
	assert(num_configs);

	// Find a config whose native visual ID is the desired GBM format.
	for (int i = 0; i < num_configs; ++i) {
		EGLint gbm_format;

		EGL_CHECK(assert(eglGetConfigAttrib(display, configs[i],
					  EGL_NATIVE_VISUAL_ID, &gbm_format) == EGL_TRUE));

		if (gbm_format == GBM_FORMAT_ARGB8888) {
			EGLConfig ret = configs[i];
			free(configs);
			return ret;
		}
	}

	// Failed to find a config with matching GBM format.
	printf("no matching GBM config found!\n");
	abort();
}

void RenderTargetInit(void)
{
	assert(epoxy_has_egl());
	assert(epoxy_has_egl_extension(EGL_NO_DISPLAY, "EGL_MESA_platform_gbm"));

	int fd = open("/dev/dri/card0", O_RDWR);
	assert(fd >= 0);

	gbm = gbm_create_device(fd);
	assert(gbm != NULL);

	EGL_CHECK(assert((display = eglGetPlatformDisplayEXT(EGL_PLATFORM_GBM_MESA, gbm, NULL)) != EGL_NO_DISPLAY));

	EGLint majorVersion;
	EGLint minorVersion;
	EGL_CHECK(assert(eglInitialize(display, &majorVersion, &minorVersion) == EGL_TRUE));
	EGL_CHECK(assert(eglBindAPI(EGL_OPENGL_ES_API) == EGL_TRUE));

	EGLConfig config = get_config();

	gs = gbm_surface_create(
		gbm, TARGET_W, TARGET_H, GBM_FORMAT_ARGB8888,
		GBM_BO_USE_LINEAR|GBM_BO_USE_SCANOUT|GBM_BO_USE_RENDERING);
	assert(gs);

	EGL_CHECK(assert((surface = eglCreatePlatformWindowSurfaceEXT(display, config, gs, NULL)) != EGL_NO_SURFACE));

	const EGLint contextAttribs[] = {
		EGL_CONTEXT_CLIENT_VERSION, 2,
		EGL_NONE
	};
	EGL_CHECK(assert((context = eglCreateContext(display, config, EGL_NO_CONTEXT, contextAttribs)) != EGL_NO_CONTEXT));
	EGL_CHECK(assert(eglMakeCurrent(display, surface, surface, context) == EGL_TRUE));
/*
	printf("====================================================\n");
	printf("EGL Version: %s\n", eglQueryString(display, EGL_VERSION));
	printf("EGL Vendor: %s\n", eglQueryString(display, EGL_VENDOR));
	printf("EGL Extensions: %s\n", eglQueryString(display, EGL_EXTENSIONS));
	printf("EGL APIs: %s\n", eglQueryString(display, EGL_CLIENT_APIS));
	printf("----------------------------------------------------\n");
	printf("GL Version: %s\n", glGetString(GL_VERSION));
	printf("GL Vendor: %s\n", glGetString(GL_VENDOR));
	printf("GL Extensions: %s\n", glGetString(GL_EXTENSIONS));
	printf("GL Renderers: %s\n", glGetString(GL_RENDERER));
	printf("====================================================\n");
*/
}

GLuint LoadShader(const char *name, GLenum type)
{
	FILE *f;
	int size;
	char *buff;
	GLuint shader;
	GLint compiled;
	const GLchar *source[1];

	assert((f = fopen(name, "r")) != NULL);

	// get file size
	fseek(f, 0, SEEK_END);
	size = ftell(f);
	fseek(f, 0, SEEK_SET);

	assert((buff = malloc(size)) != NULL);
	assert(fread(buff, 1, size, f) == size);
	source[0] = buff;
	fclose(f);
	shader = glCreateShader(type);
	glShaderSource(shader, 1, source, &size);
	glCompileShader(shader);
	free(buff);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	if (!compiled) {
		GLint infoLen = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
		if (infoLen > 1) {
			char *infoLog = malloc(infoLen);
			glGetShaderInfoLog(shader, infoLen, NULL, infoLog);
			fprintf(stderr, "Error compiling shader %s:\n%s\n", name, infoLog);
			free(infoLog);
		}
		glDeleteShader(shader);
		return 0;
	}

	return shader;
}

void InitGLES(void)
{
	GLint linked;
	GLuint vertexShader;
	GLuint fragmentShader;
	assert((vertexShader = LoadShader("vert.glsl", GL_VERTEX_SHADER)) != 0);
	assert((fragmentShader = LoadShader("frag.glsl", GL_FRAGMENT_SHADER)) != 0);
	assert((program = glCreateProgram()) != 0);
	GL_CHECK(glAttachShader(program, vertexShader));
	GL_CHECK(glAttachShader(program, fragmentShader));
	GL_CHECK(glLinkProgram(program));
	GL_CHECK(glGetProgramiv(program, GL_LINK_STATUS, &linked));
	if (!linked) {
		GLint infoLen = 0;
		GL_CHECK(glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLen));
		if (infoLen > 1) {
			char *infoLog = malloc(infoLen);
			GL_CHECK(glGetProgramInfoLog(program, infoLen, NULL, infoLog));
			fprintf(stderr, "Error linking program:\n%s\n", infoLog);
			free(infoLog);
		}
		GL_CHECK(glDeleteProgram(program));
		exit(1);
	}

	GL_CHECK(glClearColor(0, 0, 0, 0));
	GL_CHECK(glViewport(0, 0, TARGET_W, TARGET_H));

	GL_CHECK(glUseProgram(program));
}


int writeImage(char* filename, int width, int height, void *buffer, char* title)
{
	int code = 0;
	FILE *fp = NULL;
	png_structp png_ptr = NULL;
	png_infop info_ptr = NULL;

	// Open file for writing (binary mode)
	fp = fopen(filename, "wb");
	if (fp == NULL) {
		fprintf(stderr, "Could not open file %s for writing\n", filename);
		code = 1;
		goto finalise;
	}

	// Initialize write structure
	png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (png_ptr == NULL) {
		fprintf(stderr, "Could not allocate write struct\n");
		code = 1;
		goto finalise;
	}

	// Initialize info structure
	info_ptr = png_create_info_struct(png_ptr);
	if (info_ptr == NULL) {
		fprintf(stderr, "Could not allocate info struct\n");
		code = 1;
		goto finalise;
	}

	// Setup Exception handling
	if (setjmp(png_jmpbuf(png_ptr))) {
		fprintf(stderr, "Error during png creation\n");
		code = 1;
		goto finalise;
	}

	png_init_io(png_ptr, fp);

	// Write header (8 bit colour depth)
	png_set_IHDR(png_ptr, info_ptr, width, height,
		     8, PNG_COLOR_TYPE_RGB_ALPHA, PNG_INTERLACE_NONE,
		     PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

	// Set title
	if (title != NULL) {
		png_text title_text;
		title_text.compression = PNG_TEXT_COMPRESSION_NONE;
		title_text.key = "Title";
		title_text.text = title;
		png_set_text(png_ptr, info_ptr, &title_text, 1);
	}

	png_write_info(png_ptr, info_ptr);

	// Write image data
	int i;
	for (i = 0; i < height; i++)
		png_write_row(png_ptr, (png_bytep)buffer + i * width * 4);

	// End write
	png_write_end(png_ptr, NULL);

finalise:
	if (fp != NULL) fclose(fp);
	if (info_ptr != NULL) png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
	if (png_ptr != NULL) png_destroy_write_struct(&png_ptr, (png_infopp)NULL);

	return code;
}

#define MAX_TRIANGLES 10000
#define MAX_INDICES 10000

double Render(int iterations, int run)
{

#if 1
	/* Fill vertices array with random floats from -1.0 to 1.0 */
	GLfloat vertices[MAX_TRIANGLES * 3];
	for (int i = 0; i < MAX_TRIANGLES * 3; i++) {
		vertices[i] = (GLfloat)rand() / RAND_MAX * 2.0 - 1.0;
	}
#else
	GLfloat vertices[] = {
		0.4, -1.0, 0.0, //0
		0.0, -1.0, 0.0, //1
		0.0, 0.0, 0.0, //2
		0.4, -1.0, 0.0, //3
		0.3, 0.5, 0.0, //4
		0.6, 0.7, 0.0, //5
		0.8, 0.9, 0.0, //6
		0.0, 0.0, 0.0, //7
		0.0, 0.0, 0.0, //8
		0.0, 0.0, 0.0, //9
		0.0, 0.0, 0.0, //10
		0.0, 0.0, 0.0, //11
		0.0, 0.0, 0.0, //12
		0.0, 0.0, 0.0, //13
		0.0, 0.0, 0.0, //14
		0.0, 0.0, 0.0, //15
		0.4, -1.0, 0.0, //16
		-1.0, 0.8, 0.0, //17
		-1.0, -0.2, 0.0 //18
	};
#endif

/*
	printf("Vertex array:");
	for (int i = 0; i < sizeof(vertices) / sizeof(GLfloat); i++) {
		if (i % 3 == 0)
			printf("\n\t");
		printf("%f, ", vertices[i]);
	}
	printf("\n");
*/
#if 0
	/* Fill indices array with random int from 0 to MAX_TRIANGLES - 1 */
	GLuint indices[MAX_INDICES * 3];
	for (int i = 0; i < MAX_INDICES * 3; i++) {
		indices[i] = (GLuint)rand() % MAX_TRIANGLES;
		if (indices[i] > 100 && indices[i] < 120)
			indices[i] = indices[i] + 20;
		if (indices[i] > 150 && indices[i] < 160)
			indices[i] = indices[i] + 10;
		if (indices[i] > 200 && indices[i] < 220)
			indices[i] = indices[i] + 20;
		if (indices[i] > 350 && indices[i] < 380)
			indices[i] = indices[i] + 30;
		if (indices[i] > 450 && indices[i] < 460)
			indices[i] = indices[i] + 10;
		if (indices[i] > 700 && indices[i] < 720)
			indices[i] = indices[i] + 20;
	}
#else
	/* This is the *hardcoded* test index array */
	GLuint indices[] = {
		0, 1, 2,
		9997, 9998, 9999
	};
#endif

/* Sorted buffer
	GLushort indices[] = {
		0, 2, 2,
		8, 15, 50,
		51, 52, 99
	};
*/
/*
	printf("Index array:");
	for (int i = 0; i < sizeof(indices) / sizeof(GLuint); i++) {
		if (i % 3 == 0)
			printf("\n\t");
		printf("%d, ", indices[i]);
	}
	printf("\n");
*/
	GLint position;
	GLint color;
	GLuint elementbuffer;

	clock_gettime(CLOCK_MONOTONIC, &start);

	for (int i = 0; i < iterations; i++) {
		GL_CHECK(position = glGetAttribLocation(program, "positionIn"));
		GL_CHECK(glEnableVertexAttribArray(position));
		GL_CHECK(glVertexAttribPointer(position, 3, GL_FLOAT, 0, 0, vertices));

		GL_CHECK(color = glGetAttribLocation(program, "colorIn"));
		GL_CHECK(glEnableVertexAttribArray(color));
		GL_CHECK(glVertexAttribPointer(color, 3, GL_FLOAT, 0, 0, vertices));
		GL_CHECK(glClear(GL_COLOR_BUFFER_BIT));

		GL_CHECK(glGenBuffers(1, &elementbuffer));
		GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer));
		GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, 0));
		GL_CHECK(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), &indices, GL_STATIC_DRAW));
		GL_CHECK(glDrawElements(GL_TRIANGLES, sizeof(indices) / sizeof(GLuint), GL_UNSIGNED_INT, 0));
/* uncomment for 3x draw which triggers lima cache */
//		GL_CHECK(glDrawElements(GL_TRIANGLES, sizeof(indices) / sizeof(GLuint), GL_UNSIGNED_INT, 0));
//		GL_CHECK(glDrawElements(GL_TRIANGLES, sizeof(indices) / sizeof(GLuint), GL_UNSIGNED_INT, 0));

		EGL_CHECK(eglSwapBuffers(display, surface));
	}

	clock_gettime(CLOCK_MONOTONIC, &end);

//	GLubyte result[TARGET_W * TARGET_H * 4] = {0};
//	glReadPixels(0, 0, TARGET_W, TARGET_H, GL_RGBA, GL_UNSIGNED_BYTE, result);
	assert(glGetError() == GL_NO_ERROR);
//	assert(!writeImage("screenshot.png", TARGET_W, TARGET_H, result, "hello"));

	double time_taken;
//	printf("Duration for %d iterations: %d sec\n", iterations,
//                end.tv_sec - start.tv_sec);
//	printf("Duration for %d iterations: %d nsec\n", iterations,
//                end.tv_nsec - start.tv_nsec);

	time_taken = (end.tv_sec - start.tv_sec) * 1000000000; // nanosec
	time_taken = (time_taken + (end.tv_nsec - start.tv_nsec)) / 1000000000; // sec
	printf("Avarage run %04d: %10.5f ms (%10.10f s for %d iterations)\n",
                run, time_taken / iterations * 1000000, time_taken, iterations);

        return time_taken;
}

int main(void)
{
  double complete_time = 0;
  int runs = 8;
  int iterations = 5000;

  for (int i = 0; i < runs; i++) {
     RenderTargetInit();
     InitGLES();
     double tmp_time = 0;
     tmp_time = Render(iterations, i);
     complete_time += tmp_time;
  }

  printf("Complete duration for %d runs (%d * %d): %4.20f s\n", runs * iterations, runs, iterations, complete_time);
  printf("Avarage for 1 run: %10.5f ms\n", complete_time / (runs * iterations) * 1000000);
  return 0;
}
