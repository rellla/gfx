attribute vec3 positionIn;
attribute vec3 colorIn;
varying vec3 color;

void main()
{
    color = colorIn;
    gl_Position = vec4(positionIn, 1.0);
}