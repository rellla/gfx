#!/bin/sh -x

export LD_LIBRARY_PATH=/opt/prefix/lib/aarch64-linux-gnu
export LIBGL_DRIVERS_PATH=/opt/prefix/lib/aarch64-linux-gnu/dri
export GBM_DRIVERS_PATH=/opt/prefix/lib/aarch64-linux-gnu/dri
export LIMA_DEBUG=dump
export MESA_DEBUG=1
export MESA_VERBOSE=draw,api,shader
#export ST_DEBUG=pipe,draw
export MESA_LOG_FILE=mesa.log
$@
