#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <fcntl.h>
#include <unistd.h>

#include <png.h>
#include <gbm.h>

#include <epoxy/gl.h>
#include <epoxy/egl.h>


GLuint program;
GLuint program_gradient;
EGLDisplay display;
EGLSurface surface;
EGLContext context;
struct gbm_device *gbm;
struct gbm_surface *gs;

#define TARGET_W 512
#define TARGET_H 256

void eglCheckError(const char *stmt, const char *fname, int line) {
	EGLint err = eglGetError();
	if (err != EGL_SUCCESS)
		printf("EGL ERROR (0x%08x): %s failed at %s:%i\n", err, stmt, fname, line);
}

void glCheckError(const char *stmt, const char *fname, int line) {
	GLint err = glGetError();
	if (err != GL_NO_ERROR)
		printf("GL ERROR (0x%08x): %s failed at %s:%i\n", err, stmt, fname, line);
}

#define EGL_CHECK(stmt) do { \
	stmt; \
	eglCheckError(#stmt, __FILE__, __LINE__); \
	} while (0)

#define GL_CHECK(stmt) do { \
	stmt; \
	glCheckError(#stmt, __FILE__, __LINE__); \
	} while (0)

EGLConfig get_config(void)
{
	EGLint egl_config_attribs[] = {
		EGL_BUFFER_SIZE,	32,
		EGL_DEPTH_SIZE,		EGL_DONT_CARE,
		EGL_STENCIL_SIZE,	EGL_DONT_CARE,
		EGL_SURFACE_TYPE,	EGL_WINDOW_BIT,
		EGL_RENDERABLE_TYPE,	EGL_OPENGL_ES2_BIT,
		EGL_NONE,
	};

	EGLint num_configs;
	EGL_CHECK(assert(eglGetConfigs(display, NULL, 0, &num_configs) == EGL_TRUE));

	EGLConfig *configs = malloc(num_configs * sizeof(EGLConfig));
	EGL_CHECK(assert(eglChooseConfig(display, egl_config_attribs,
			       configs, num_configs, &num_configs) == EGL_TRUE));
	assert(num_configs);

	// Find a config whose native visual ID is the desired GBM format.
	for (int i = 0; i < num_configs; ++i) {
		EGLint gbm_format;

		EGL_CHECK(assert(eglGetConfigAttrib(display, configs[i],
					  EGL_NATIVE_VISUAL_ID, &gbm_format) == EGL_TRUE));

		if (gbm_format == GBM_FORMAT_ARGB8888) {
			EGLConfig ret = configs[i];
			free(configs);
			return ret;
		}
	}

	// Failed to find a config with matching GBM format.
	printf("no matching GBM config found!\n");
	abort();
}

void RenderTargetInit(void)
{
	assert(epoxy_has_egl());
	assert(epoxy_has_egl_extension(EGL_NO_DISPLAY, "EGL_MESA_platform_gbm"));

	int fd = open("/dev/dri/card0", O_RDWR);
	assert(fd >= 0);

	gbm = gbm_create_device(fd);
	assert(gbm != NULL);

	EGL_CHECK(assert((display = eglGetPlatformDisplayEXT(EGL_PLATFORM_GBM_MESA, gbm, NULL)) != EGL_NO_DISPLAY));

	EGLint majorVersion;
	EGLint minorVersion;
	EGL_CHECK(assert(eglInitialize(display, &majorVersion, &minorVersion) == EGL_TRUE));
	EGL_CHECK(assert(eglBindAPI(EGL_OPENGL_ES_API) == EGL_TRUE));

	EGLConfig config = get_config();

	gs = gbm_surface_create(
		gbm, TARGET_W, TARGET_H, GBM_FORMAT_ARGB8888,
		GBM_BO_USE_LINEAR|GBM_BO_USE_SCANOUT|GBM_BO_USE_RENDERING);
	assert(gs);

	EGL_CHECK(assert((surface = eglCreatePlatformWindowSurfaceEXT(display, config, gs, NULL)) != EGL_NO_SURFACE));

	const EGLint contextAttribs[] = {
		EGL_CONTEXT_CLIENT_VERSION, 2,
		EGL_NONE
	};
	EGL_CHECK(assert((context = eglCreateContext(display, config, EGL_NO_CONTEXT, contextAttribs)) != EGL_NO_CONTEXT));
	EGL_CHECK(assert(eglMakeCurrent(display, surface, surface, context) == EGL_TRUE));
/*
	printf("====================================================\n");
	printf("EGL Version: %s\n", eglQueryString(display, EGL_VERSION));
	printf("EGL Vendor: %s\n", eglQueryString(display, EGL_VENDOR));
	printf("EGL Extensions: %s\n", eglQueryString(display, EGL_EXTENSIONS));
	printf("EGL APIs: %s\n", eglQueryString(display, EGL_CLIENT_APIS));
	printf("----------------------------------------------------\n");
	printf("GL Version: %s\n", glGetString(GL_VERSION));
	printf("GL Vendor: %s\n", glGetString(GL_VENDOR));
	printf("GL Extensions: %s\n", glGetString(GL_EXTENSIONS));
	printf("GL Renderers: %s\n", glGetString(GL_RENDERER));
	printf("====================================================\n");
*/
}

GLuint LoadShader(const char *name, GLenum type)
{
	FILE *f;
	int size;
	char *buff;
	GLuint shader;
	GLint compiled;
	const GLchar *source[1];

	assert((f = fopen(name, "r")) != NULL);

	// get file size
	fseek(f, 0, SEEK_END);
	size = ftell(f);
	fseek(f, 0, SEEK_SET);

	assert((buff = malloc(size)) != NULL);
	assert(fread(buff, 1, size, f) == size);
	source[0] = buff;
	fclose(f);
	shader = glCreateShader(type);
	glShaderSource(shader, 1, source, &size);
	glCompileShader(shader);
	free(buff);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	if (!compiled) {
		GLint infoLen = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
		if (infoLen > 1) {
			char *infoLog = malloc(infoLen);
			glGetShaderInfoLog(shader, infoLen, NULL, infoLog);
			fprintf(stderr, "Error compiling shader %s:\n%s\n", name, infoLog);
			free(infoLog);
		}
		glDeleteShader(shader);
		return 0;
	}

	return shader;
}

void InitGLES(void)
{
	GLint linked;
	GLuint vertexShader;
	GLuint fragmentShader;
	GLuint vertexShader_gradient;
	GLuint fragmentShader_gradient;
	assert((vertexShader = LoadShader("vert_texture.glsl", GL_VERTEX_SHADER)) != 0);
	assert((fragmentShader = LoadShader("frag_texture.glsl", GL_FRAGMENT_SHADER)) != 0);
	assert((vertexShader_gradient = LoadShader("vert_gradient.glsl", GL_VERTEX_SHADER)) != 0);
	assert((fragmentShader_gradient = LoadShader("frag_gradient.glsl", GL_FRAGMENT_SHADER)) != 0);
	assert((program = glCreateProgram()) != 0);
	assert((program_gradient = glCreateProgram()) != 0);
	GL_CHECK(glAttachShader(program, vertexShader));
	GL_CHECK(glAttachShader(program, fragmentShader));
	GL_CHECK(glAttachShader(program_gradient, vertexShader_gradient));
	GL_CHECK(glAttachShader(program_gradient, fragmentShader_gradient));

	GL_CHECK(glLinkProgram(program));
	GL_CHECK(glLinkProgram(program_gradient));

	GL_CHECK(glGetProgramiv(program, GL_LINK_STATUS, &linked));
	if (!linked) {
		GLint infoLen = 0;
		GL_CHECK(glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLen));
		if (infoLen > 1) {
			char *infoLog = malloc(infoLen);
			GL_CHECK(glGetProgramInfoLog(program, infoLen, NULL, infoLog));
			fprintf(stderr, "Error linking program:\n%s\n", infoLog);
			free(infoLog);
		}
		GL_CHECK(glDeleteProgram(program));
		exit(1);
	}

	GL_CHECK(glGetProgramiv(program_gradient, GL_LINK_STATUS, &linked));
	if (!linked) {
		GLint infoLen = 0;
		GL_CHECK(glGetProgramiv(program_gradient, GL_INFO_LOG_LENGTH, &infoLen));
		if (infoLen > 1) {
			char *infoLog = malloc(infoLen);
			GL_CHECK(glGetProgramInfoLog(program_gradient, infoLen, NULL, infoLog));
			fprintf(stderr, "Error linking program:\n%s\n", infoLog);
			free(infoLog);
		}
		GL_CHECK(glDeleteProgram(program_gradient));
		exit(1);
	}

	GL_CHECK(glClearColor(0, 0, 0, 0));
	GL_CHECK(glViewport(0, 0, TARGET_W, TARGET_H));

	// First use the gradient shader
	GL_CHECK(glUseProgram(program_gradient));
}

int writeImage(char* filename, int width, int height, void *buffer, char* title)
{
	int code = 0;
	FILE *fp = NULL;
	png_structp png_ptr = NULL;
	png_infop info_ptr = NULL;

	// Open file for writing (binary mode)
	fp = fopen(filename, "wb");
	if (fp == NULL) {
		fprintf(stderr, "Could not open file %s for writing\n", filename);
		code = 1;
		goto finalise;
	}

	// Initialize write structure
	png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (png_ptr == NULL) {
		fprintf(stderr, "Could not allocate write struct\n");
		code = 1;
		goto finalise;
	}

	// Initialize info structure
	info_ptr = png_create_info_struct(png_ptr);
	if (info_ptr == NULL) {
		fprintf(stderr, "Could not allocate info struct\n");
		code = 1;
		goto finalise;
	}

	// Setup Exception handling
	if (setjmp(png_jmpbuf(png_ptr))) {
		fprintf(stderr, "Error during png creation\n");
		code = 1;
		goto finalise;
	}

	png_init_io(png_ptr, fp);

	// Write header (8 bit colour depth)
	png_set_IHDR(png_ptr, info_ptr, width, height,
		     8, PNG_COLOR_TYPE_RGB_ALPHA, PNG_INTERLACE_NONE,
		     PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

	// Set title
	if (title != NULL) {
		png_text title_text;
		title_text.compression = PNG_TEXT_COMPRESSION_NONE;
		title_text.key = "Title";
		title_text.text = title;
		png_set_text(png_ptr, info_ptr, &title_text, 1);
	}

	png_write_info(png_ptr, info_ptr);

	// Write image data
	int i;
	for (i = 0; i < height; i++)
		png_write_row(png_ptr, (png_bytep)buffer + i * width * 4);

	// End write
	png_write_end(png_ptr, NULL);

finalise:
	if (fp != NULL) fclose(fp);
	if (info_ptr != NULL) png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
	if (png_ptr != NULL) png_destroy_write_struct(&png_ptr, (png_infopp)NULL);

	return code;
}

void fillwithgrid(char *data, int level_w, int level_h, int cellsize, int colora, int colorb, int alpha)
{
	int c = 0;

	int colors[8] = {
		0xff0000, // 0 red
		0x00ff00, // 1 green
		0x0000ff, // 2 blue
		0xffff00, // 3 yellow
		0x00ffff, // 4 cyan
		0xff00ff, // 5 magenta
		0x787800, // 6 gold
		0x787800  // 7 pink
	};

	for (int y = 0; y < level_h; y++) {
		for (int x = 0; x < level_w; x++) {

			int mx = (x / cellsize) % 2;
			int my = (y / cellsize) % 2;

			if (mx ^ my) {
				data[c + 0] = colors[colora] >> 16;
				data[c + 1] = (colors[colora] >> 8) & 0xff;
				data[c + 2] = colors[colora] & 0xff;
				if (alpha)
					data[c + 3] = 255;
			}
			else {
				data[c + 0] = colorb;
				data[c + 1] = colorb;
				data[c + 2] = colorb;
				if (alpha)
					data[c + 3] = 255;
			}
			c += 3 + alpha;
		}
	}
}

int max(int a, int b) {
	if (a > b)
		return a;
	else
		return b;
}

void Render(void)
{
	int numlevels = 8;
	GLuint tex = 0;

	// Create full texture and mipmap levels as grids with different colors
	GL_CHECK(glGenTextures(1, &tex));
	GL_CHECK(glBindTexture(GL_TEXTURE_2D, tex));
	GL_CHECK(glPixelStorei(GL_UNPACK_ALIGNMENT, 1));
	GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 7));
	GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
	GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
	GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST));
	GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));

	for (int idx = 0; idx < numlevels; idx++) {
		int level_w = max(1, TARGET_W >> idx);
		int level_h = max(1, TARGET_H >> idx);

		unsigned char data[level_w * level_h * 4];
		fillwithgrid(data, level_w, level_h, 32 >> idx, idx, 255, 1);

		// Create texture dumps for each mipmap level to verify, that they are right
		char filename[20];
		snprintf(filename, sizeof(filename), "texture_%d.png", idx);
		assert(!writeImage(filename, level_w, level_h, data, "hello"));

		GL_CHECK(glTexImage2D(GL_TEXTURE_2D, idx, GL_RGBA, level_w, level_h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data));
	}

	// Draw gradient quad to render target
	int level_w = TARGET_W;
	int level_h = TARGET_H;

	GLfloat vertices[] = {
		1.0f,	-1.0f,	0.0f, // 0 bottom right
		1.0f,	1.0f,	0.0f, // 1 top right
		-1.0f, 	1.0f,	0.0f, // 2 top left
		-1.0f,	-1.0f,	0.0f  // 3 bottom left
	};

	GLfloat texcoords[] = {
		1.0f, 0.0f, // bottom right
		1.0f, 1.0f, // top right
		0.0f, 1.0f, // top left
		0.0f, 0.0f  // bottom left
	};

	GLuint indices[] = {
		0, 1, 2,
		2, 3, 0
	};

	GLint position;
	GL_CHECK(position = glGetAttribLocation(program, "positionIn"));
	GL_CHECK(glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 0, vertices));
	GL_CHECK(glEnableVertexAttribArray(position));

	GLint coords;
	GL_CHECK(coords = glGetAttribLocation(program, "coordsIn"));
	GL_CHECK(glVertexAttribPointer(coords, 2, GL_FLOAT, GL_FALSE, 0, texcoords));
	GL_CHECK(glEnableVertexAttribArray(coords));

	GL_CHECK(glClear(GL_COLOR_BUFFER_BIT));

	GLuint elementbuffer;
	GL_CHECK(glGenBuffers(1, &elementbuffer));
	GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer));
	GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, 0));
	GL_CHECK(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), &indices, GL_STATIC_DRAW));

	GL_CHECK(glDrawElements(GL_TRIANGLES, sizeof(indices) / sizeof(GLuint), GL_UNSIGNED_INT, 0));

	EGL_CHECK(eglSwapBuffers(display, surface));

	// Dump buffer into to a png to verify gradient draw result
	GLubyte result[level_w * level_h * 4];
	glReadPixels(0, 0, level_w, level_h, GL_RGBA, GL_UNSIGNED_BYTE, &result);
	assert(glGetError() == GL_NO_ERROR);
	char filename[20];
	snprintf(filename, sizeof(filename), "gradient.png");
	assert(!writeImage(filename, level_w, level_h, &result, "hello"));

	// Re-specify parts of some mipmap levels with part of the gradient draw
	GL_CHECK(glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 20, 20, 0, 0, 50, 50));
	GL_CHECK(glCopyTexSubImage2D(GL_TEXTURE_2D, 2, 20, 20, 0, 0, 20, 20));

	// Now use the texture shader and do a draw with each mipmap
	GL_CHECK(glUseProgram(program));
	for (int idx = 0; idx < numlevels; idx++) {
		int level_w = max(1, TARGET_W >> idx);
		int level_h = max(1, TARGET_H >> idx);

		float w = (float)level_w / (float)TARGET_W;
		float h = (float)level_h / (float)TARGET_H;

		GLfloat vertices[] = {
			-1.0f + (w * 2.0f),	-1.0f, 			0.0f, // 0 bottom right
			-1.0f + (w * 2.0f),	-1.0f + (h * 2.0f),	0.0f, // 1 top right
			-1.0f, 			-1.0f + (h * 2.0f),	0.0f, // 2 top left
			-1.0f,			-1.0f,			0.0f  // 3 bottom left
		};

		GLfloat texcoords[] = {
			1.0f, 0.0f, // bottom right
			1.0f, 1.0f, // top right
			0.0f, 1.0f, // top left
			0.0f, 0.0f  // bottom left
		};

		GLuint indices[] = {
			0, 1, 2,
			2, 3, 0
		};

		GLint position;
		GL_CHECK(position = glGetAttribLocation(program, "positionIn"));
		GL_CHECK(glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 0, vertices));
		GL_CHECK(glEnableVertexAttribArray(position));

		GLint coords;
		GL_CHECK(coords = glGetAttribLocation(program, "coordsIn"));
		GL_CHECK(glVertexAttribPointer(coords, 2, GL_FLOAT, GL_FALSE, 0, texcoords));
		GL_CHECK(glEnableVertexAttribArray(coords));

		GLint location;
		GL_CHECK(location = glGetUniformLocation(program, "sampler"));
		GL_CHECK(glUniform1i(location, 0));

		GL_CHECK(glClear(GL_COLOR_BUFFER_BIT));

		GLuint elementbuffer;
		GL_CHECK(glGenBuffers(1, &elementbuffer));
		GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer));
		GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, 0));
		GL_CHECK(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), &indices, GL_STATIC_DRAW));

		GL_CHECK(glDrawElements(GL_TRIANGLES, sizeof(indices) / sizeof(GLuint), GL_UNSIGNED_INT, 0));

		EGL_CHECK(eglSwapBuffers(display, surface));

		// Dump result into screenshot_*.png
		GLubyte result[level_w * level_h * 4];
		glReadPixels(0, 0, level_w, level_h, GL_RGBA, GL_UNSIGNED_BYTE, &result);
		assert(glGetError() == GL_NO_ERROR);
		char filename[20];
		snprintf(filename, sizeof(filename), "screenshot_%d.png", idx);
		assert(!writeImage(filename, level_w, level_h, &result, "hello"));
	}

	return;
}

int main(void)
{
  RenderTargetInit();
  InitGLES();
  Render();

  return 0;
}
