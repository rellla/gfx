varying mediump vec2 texCoord;

void main() {
     mediump float x = texCoord.x;
     mediump float y = texCoord.y;
     mediump float f0 = (x + y) * 0.5;
     mediump float f1 = 0.5 + (x - y) * 0.5;

     gl_FragColor = vec4(f0, f1, 1.0 - f0, 1.0 - f1);
}
