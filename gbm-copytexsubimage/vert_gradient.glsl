attribute highp vec3 positionIn;
attribute mediump vec2 coordsIn;

varying mediump vec2 texCoord;

void main()
{
    texCoord = coordsIn;
    gl_Position = vec4(positionIn, 1.0);
}